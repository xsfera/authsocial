//
//  authSocial.m
//  authSocial
//
//  Created by Евгений Малаховский on 19/04/2019.
//  Copyright © 2019 xsfera. All rights reserved.
//

#import "authSocial.h"

@implementation authSocial

NSString *global_appid;
NSString *global_secret;
static void (^tokenResult)(NSString *);

static authSocial *sharedMySingleton = NULL;

+(authSocial *)share {
    if (!sharedMySingleton || sharedMySingleton == NULL) {
        sharedMySingleton = [authSocial new];
    }
    return sharedMySingleton;
}

-(NSDictionary*)stringURLToDictonary:(NSString*)str {
    
    if ([str hasPrefix:@"/#"]) {
        str = [str substringFromIndex:2];
    }
    
    NSArray *params = [str componentsSeparatedByString:@"&"];

    NSMutableDictionary *resultParams = [NSMutableDictionary new];
    for (NSString *item in params) {
        NSArray *keyValue = [item componentsSeparatedByString:@"="];
        if (keyValue.count == 2) {
            resultParams[keyValue[0]] = keyValue[1];
        }
    }
    return resultParams;
}

-(void) processOpenURL:(NSURL*)url {
    NSString *inputStr = url.absoluteString;
    NSLog(@"%@", inputStr);
    NSString *paramStr;
    
    NSString *prefixVK = [NSString stringWithFormat:@"vk%@://authorize?#", global_appid];
    NSString *prefixYandex = [NSString stringWithFormat:@"yx%@://authorize?code=", global_appid];
    NSString *prefixOK = [NSString stringWithFormat:@"ok%@://authorize#", global_appid];
    NSString *prefixFB = [NSString stringWithFormat:@"fb%@://authorize", global_appid];
    NSString *prefixGoogle = [NSString stringWithFormat:@"com.googleusercontent.apps.%@:?code=", global_appid];
    NSString *prefixMicrosoft = [NSString stringWithFormat:@"msal%@://auth?code=", global_appid];
    
    if ([inputStr hasPrefix:prefixVK]) { // Авторизация ВКонтакте
        paramStr = [inputStr substringFromIndex:prefixVK.length];
        NSDictionary *paramsInfo = [self stringURLToDictonary:paramStr];
        if (tokenResult && paramsInfo[@"access_token"])
            tokenResult(paramsInfo[@"access_token"]);
    } else if ([inputStr hasPrefix:prefixYandex]) { // Авторизация Яндекс
        paramStr = [inputStr substringFromIndex:prefixYandex.length];
        NSDictionary *params = @{
            @"grant_type": @"authorization_code",
            @"code": paramStr,
            @"client_id": global_appid,
            @"client_secret": global_secret
        };
        [self requestServer:@"https://oauth.yandex.ru/token" params:params finish:^(NSDictionary *result) {
            if (tokenResult && result[@"access_token"])
                tokenResult(result[@"access_token"]);
        }];
    } else if ([inputStr hasPrefix:prefixOK]) { // Авторизация Одноклассники
        paramStr = [inputStr substringFromIndex:prefixOK.length];
        NSDictionary *paramsInfo = [self stringURLToDictonary:paramStr];
        if (tokenResult && paramsInfo[@"access_token"])
            tokenResult(paramsInfo[@"access_token"]);
    } else if ([inputStr hasPrefix:prefixFB]) { // Авторизация Facebook
        paramStr = [inputStr substringFromIndex:prefixFB.length];
        NSDictionary *paramsInfo = [self stringURLToDictonary:paramStr];
        if (tokenResult && paramsInfo[@"access_token"])
            tokenResult(paramsInfo[@"access_token"]);
    } else if ([inputStr hasPrefix:prefixGoogle]) { // Авторизация Google
        paramStr = [inputStr substringFromIndex:prefixGoogle.length];
        paramStr = [paramStr substringToIndex:[paramStr rangeOfString:@"&"].location];
        NSDictionary *params = @{
                                 @"grant_type": @"authorization_code",
                                 @"code": paramStr,
                                 @"client_id": global_appid,
                                 @"redirect_uri": [NSString stringWithFormat:@"com.googleusercontent.apps.%@:", global_appid]
                                 };
        [self requestServer:@"https://www.googleapis.com/oauth2/v4/token" params:params finish:^(NSDictionary *result) {
            if (tokenResult && result[@"access_token"])
                tokenResult(result[@"access_token"]);
        }];
    } else if ([inputStr hasPrefix:prefixMicrosoft]) { // Авторизация Microsoft
        paramStr = [inputStr substringFromIndex:prefixMicrosoft.length];
        NSDictionary *params = @{
                                 @"grant_type": @"authorization_code",
                                 @"code": paramStr,
                                 @"client_id": global_appid
                                 };
        [self requestServer:@"https://login.microsoftonline.com/common/oauth2/v2.0/token" params:params finish:^(NSDictionary *result) {
            if (tokenResult && result[@"access_token"])
                tokenResult(result[@"access_token"]);
        }];
    }
}

-(void)auth:(authType)type appID:(NSString*)appid finish:(void(^)(NSString* token))finish {
    [self auth:type appID:appid secret:nil finish:^(NSString *token) {
        finish(token);
    }];
}
    
-(void)auth:(authType)type appID:(NSString*)appid secret:(nullable NSString*)secret finish:(void(^)(NSString* token))finish {
    NSString *autorizeURL;
    NSString *autorizeURLWeb;
    NSString *templateURL;
    global_appid = appid;
    global_secret = secret;
    tokenResult = finish;
    
    if (type == authTypeYandex) {
        autorizeURL = @"yandexauth2://authorize";
        autorizeURLWeb = @"https://oauth.yandex.ru/authorize";
        templateURL = @"%@?response_type=code&client_id=%@&redirect_uri=yx%@://authorize";
    } else if (type == authTypeVK) {
        autorizeURL = @"vkauthorize://authorize";
        autorizeURLWeb = @"https://oauth.vk.com/authorize";
        templateURL = @"%@?client_id=%@&revoke=1&v=5.40&&redirect_uri=vk%@://authorize?&&response_type=token";
    } else if (type == authTypeOK) {
        autorizeURL = @"okauth://authorize";
        autorizeURLWeb = @"https://connect.ok.ru/oauth/authorize";
        templateURL = @"%@?client_id=%@&scope=VALUABLE_ACCESS&redirect_uri=ok%@://authorize&response_type=token";
    } else if (type == authTypeFacebook) {
        autorizeURL = @"fbauth2://authorize";
        autorizeURLWeb = @"https://www.facebook.com/v3.2/dialog/oauth";
        templateURL = @"%@?client_id=%@&redirect_uri=fb%@://authorize&response_type=token";
    } else if (type == authTypeGoogle) {
        autorizeURL = @"https://accounts.google.com/o/oauth2/v2/auth";
        autorizeURLWeb = @"https://accounts.google.com/o/oauth2/v2/auth";
        templateURL = @"%@?scope=email profile&client_id=%@.apps.googleusercontent.com&response_type=code&redirect_uri=com.googleusercontent.apps.%@:";
    } else if (type == authTypeMicrosoft) {
        autorizeURL = @"https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
        autorizeURLWeb = @"https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
        templateURL = @"%@?client_id=%@&response_type=code&redirect_uri=msal%@://auth&response_mode=query&scope=openid";
    }
    
    if (templateURL) {
        NSString *urlStr = [NSString stringWithFormat:templateURL, autorizeURL,appid, appid];
        urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        UIApplication *app = [UIApplication sharedApplication];
        if (![app canOpenURL:[NSURL URLWithString:autorizeURL]])
            urlStr = [NSString stringWithFormat:templateURL, autorizeURLWeb,appid, appid];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        #ifdef __AVAILABILITY_INTERNAL__IPHONE_10_0_DEP__IPHONE_10_0
            if ([app respondsToSelector:@selector(openURL:options:completionHandler:)])
                [app openURL:url options:@{} completionHandler:^(BOOL success) {}];
            else
                [app openURL:url];
        #else
            [app openURL:url];
        #endif
    }
}

-(NSData*)paramsToData:(NSDictionary*)params {
    NSMutableArray *result = [NSMutableArray new];
    for (NSString *key in params) {
        NSString *value = params[key];
        [result addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
    }
    
    NSString *resultTemp = [result componentsJoinedByString:@"&"];
    return [resultTemp dataUsingEncoding:NSUTF8StringEncoding];
}

-(void)requestServer:(NSString*)url params:(NSDictionary*)params finish:(void(^)(NSDictionary* result))finish {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    if (params) {
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[self paramsToData:params]];
    }
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSError *errorJson;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&errorJson];
        
        finish(result);
    }];
    [task resume];
}
    
@end
