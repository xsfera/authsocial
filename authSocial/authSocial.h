//
//  authSocial.h
//  authSocial
//
//  Created by Евгений Малаховский on 19/04/2019.
//  Copyright © 2019 xsfera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface authSocial : NSObject

typedef enum {
    authTypeYandex,
    authTypeVK,
    authTypeOK,
    authTypeFacebook,
    authTypeGoogle,
    authTypeMicrosoft
} authType;

+(authSocial *)share;

-(void) processOpenURL:(NSURL*)url;

-(void) auth:(authType)type appID:(NSString*)appid finish:(void(^)(NSString* token))finish;
-(void)auth:(authType)type appID:(NSString*)appid secret:(nullable NSString*)secret finish:(void(^)(NSString* token))finish;

@end

NS_ASSUME_NONNULL_END
