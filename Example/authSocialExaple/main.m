//
//  main.m
//  authSocial
//
//  Created by Евгений Малаховский on 19/04/2019.
//  Copyright © 2019 xsfera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
