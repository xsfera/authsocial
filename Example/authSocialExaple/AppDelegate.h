//
//  AppDelegate.h
//  authSocial
//
//  Created by Евгений Малаховский on 19/04/2019.
//  Copyright © 2019 xsfera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

