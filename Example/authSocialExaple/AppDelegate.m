//
//  AppDelegate.m
//  authSocial
//
//  Created by Евгений Малаховский on 19/04/2019.
//  Copyright © 2019 xsfera. All rights reserved.
//

#import "AppDelegate.h"
#import <authSocial/authSocial.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

//iOS 9 и выше
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    [[authSocial share] processOpenURL:url];
    return YES;
}

//iOS 8 и ниже
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    [[authSocial share] processOpenURL:url];
    return YES;
}

@end
