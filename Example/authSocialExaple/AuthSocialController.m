//
//  AuthSocialController.m
//  
//
//  Created by Евгений Малаховский on 15/04/2019.
//

#import "AuthSocialController.h"
#import <authSocial/authSocial.h>

@interface AuthSocialController ()

@end

@implementation AuthSocialController {
    NSArray *array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array = @[
              @"Яндекс",
              @"ВКонтакте",
              @"Одноклассники",
              @"Facebook",
              @"Google",
              @"Microsoft"
              ];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];    
    cell.textLabel.text = array[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==authTypeYandex)
        [[authSocial share] auth:authTypeYandex appID:@"80db6aa164e34e80a968cb3627f001a0"
        secret:@"8ed0ec8f92484107bc8cf26435dc2ff6" finish:^(NSString * _Nonnull token) {
            NSLog(@"token Яндекс: %@", token);
        }];
    else if (indexPath.row==authTypeVK)
        [[authSocial share] auth:authTypeVK appID:@"6954125" finish:^(NSString * _Nonnull token) {
            NSLog(@"token ВКонтакте: %@", token);
        }];
    else if (indexPath.row==authTypeOK)
        [[authSocial share] auth:authTypeOK appID:@"1278147072" finish:^(NSString * _Nonnull token) {
            NSLog(@"token Одноклассники: %@", token);
        }];
    else if (indexPath.row==authTypeFacebook)
        [[authSocial share] auth:authTypeFacebook appID:@"1556984744438759" finish:^(NSString * _Nonnull token) {
            NSLog(@"token Facebook: %@", token);
        }];
    else if (indexPath.row==authTypeGoogle)
        [[authSocial share] auth:authTypeGoogle appID:@"991228963133-t1s59c3ue8hpe95h650dain2k2s3usck" finish:^(NSString * _Nonnull token) {
            NSLog(@"token Google: %@", token);
        }];
    else if (indexPath.row==authTypeMicrosoft)
        [[authSocial share] auth:authTypeMicrosoft appID:@"ae0c4798-7245-4325-94e8-c63f6ccee56b" finish:^(NSString * _Nonnull token) {
            NSLog(@"token Microsoft: %@", token);
        }];
}

@end
